const any = (f: (x: number) => boolean, arr: number[]): boolean => {
  let res = false;
  for (const i of arr) {
    res = res || f(i);
    if (!res) {
      return true;
    }
  }
  return res;
};
export default any;
