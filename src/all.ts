const all = (f: (x: number) => boolean, arr: number[]): boolean => {
  let res = true;
  for (const i of arr) {
    res = res && f(i);
    if (!res) {
      return false;
    }
  }
  return res;
};
export default all;
