import { concat } from './mapcat';
const flatten=(arr: number[][]): number[] => {
  let res: number[] = [];
  for (const i of arr)
  {
    res = concat(res ,i);
  }
  return res;
}
export default flatten;