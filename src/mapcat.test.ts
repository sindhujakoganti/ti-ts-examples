import { concat, concat2, inc, map, mapcat } from './mapcat';
test('concat', () => {
  expect(concat([1, 2, 3],[4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
});

test('concat2', () => {
  expect(concat2([[1, 2, 3],[4, 5, 6]])).toEqual([1, 2, 3, 4, 5, 6]);
});

test('inc', () => {
  expect(inc(1)).toEqual(2);
});
test('map', () => {
  expect(map(inc,[4, 5, 6])).toEqual([5, 6, 7]);
});

test('mapcat', () => {
  expect(mapcat(map,[[1, 2, 3],[4, 5 ,6]])).toEqual([1, 2, 3, 4, 5, 6]);
})