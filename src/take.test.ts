import take from './take';
test('take', () => {
  expect(take(3, [1, 2, 3, 4, 5, 6, 7])).toEqual([1, 2, 3]);
  expect(take(4, [1, 2, 3, 4, 5, 6, 7])).toEqual([1, 2, 3, 4]);
});
