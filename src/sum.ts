const sumAll = (arr: number[]): number => {
  let res = 0;
  for (const i of arr) {
    res += i;
  }
  return res;
};

export default sumAll;
