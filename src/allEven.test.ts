import allEven from './allEven';
test('allEven', () => {
  expect(allEven([1, 2, 3, 4, 5])).toEqual([2, 4]);
});
