const filter = (f: (x: number) => boolean, arr: number[]): number[] => {
  const res = [];
  for (const i of arr) {
    if (f(i)) {
      res.push(i);
    }
  }
  return res;
};
export default filter;
