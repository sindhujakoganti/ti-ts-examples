import drop from './drop';
test('drop', () => {
  expect(drop(3, [1, 2, 3, 4, 5, 6, 7])).toEqual([4, 5, 6, 7]);
  expect(drop(5, [1, 2, 3, 4, 5, 6, 7])).toEqual([6, 7]);
});
