export { concat, concat2, map, inc, mapcat }; 
const concat=(arr1: number[], arr2: number[]): number[]=> {
  const result = [];
  for (const i of arr1){
   result.push(i);
  } 
  for (const j of arr2)
  {
    result.push(j);
  }
 return result;
};


const concat2=(arr: number[][]): number[] => {
  let res: number[] = [];
  for (const i of arr)
  {
    res = concat(res ,i);
  } 
  return res;
}

const inc=(n: number): number => {
  return n += 1;
}

const map=(f: (x: number)=> number,arr: number[]): number[] => {
 const result: number[] = [];
  for (const i of arr)
  {
     result.push(f(i)); 
  } 
  return result;
}

const mapcat=(f: (x: number)=> number[],arr: number[]): number[] => {
  let result: number[] =[];
   for (const i of arr)
   {
     result = concat(result, i);
   }
   return result;
}