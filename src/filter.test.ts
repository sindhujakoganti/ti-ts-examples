import filter from './filter';
test('filter', () => {
  expect(filter(x => x % 2 === 0, [1, 2, 3, 4, 5, 6])).toEqual([2, 4, 6]);
});
