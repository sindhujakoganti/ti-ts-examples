const gcd = (x: number, y: number): number => {
  if (!y) {
    return x;
  }
  return gcd(y, x % y);
};
export default gcd;
