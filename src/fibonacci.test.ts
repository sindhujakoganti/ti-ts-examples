import fibonacci from './fibonacci';

test('fibonacci', () => {
  expect(fibonacci(5)).toEqual([1, 1, 2, 3, 5]);
});