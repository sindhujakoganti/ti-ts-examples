import any from './any';
import isEven from './isEven';
test('any', () => {
  expect(any(isEven, [1, 3, 5])).toEqual(false);
  expect(any(isEven, [2, 3, 5])).toEqual(true);
  expect(any(isEven, [1, 2, 5])).toEqual(true);
  expect(any(isEven, [2, 4, 6])).toEqual(true);
});
