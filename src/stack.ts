export default class Stack {
  private arr: number[];
  constructor() {
    this.arr = [];
  }
  push = (x: number): number => {
    return this.arr.push(x);
  };
  pop = () => {
    return this.arr.pop();
  };
  isEmpty = () => {
    return this.arr.length === 0;
  };
  count = () => {
    return this.arr.length;
  };
}
