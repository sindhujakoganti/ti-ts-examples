const fibonacci=(n: number): number[] => {
    const arr= [];
    let a= 1;
    let b= 1;
     arr[0]=a;
     arr[1]=b;
    for (let i= 2; i < n ; i += 1) {
    arr[i]=a + b;
    a = b;
    b = arr[i];
    }
    return arr;
}
export default fibonacci;
