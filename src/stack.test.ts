import Stack from './stack';
test('Stack', () => {
  const s = new Stack();
  expect(s.push(10)).toEqual(1);
  expect(s.push(20)).toEqual(2);
  expect(s.push(30)).toEqual(3);
  expect(s.pop()).toEqual(30);
  expect(s.pop()).toEqual(20);
  expect(s.pop()).toEqual(10);
  expect(s.isEmpty()).toBeTruthy();
  expect(s.count()).toEqual(0);
});
