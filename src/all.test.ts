import all from './all';
import isEven from './isEven';
test('all', () => {
  expect(all(isEven, [2, 4, 6])).toEqual(true);
  expect(all(isEven, [2, 1, 5])).toEqual(false);
  expect(all(isEven, [1, 4, 7])).toEqual(false);
  expect(all(isEven, [1, 3, 5])).toEqual(false);
});
