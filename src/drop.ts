const drop = (start: number, arr: number[]): number[] => {
  const res = [];
  for (let i = start; i < arr.length; i++) {
    res.push(arr[i]);
  }
  return res;
};
export default drop;
