const isEven = (x: number): boolean => x % 2 === 0;
export default isEven;
