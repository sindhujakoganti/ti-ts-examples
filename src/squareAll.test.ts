import squareAll from './squareAll';
test('squareAll', () => {
  expect(squareAll([1, 2, 3])).toEqual([1, 4, 9]);
});
