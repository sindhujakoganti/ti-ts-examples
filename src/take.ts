const take = (start: number, arr: number[]): number[] => {
  const res = [];
  for (let i = 0; i < start; i++) {
    res.push(arr[i]);
  }
  return res;
};
export default take;
