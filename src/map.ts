const map = (f: (x: number) => number, arr: number[]): number[] => {
  const res = [];
  for (const i of arr) {
    res.push(f(i));
  }
  return res;
};
export default map;
