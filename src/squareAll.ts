const squareAll = (arr: number[]): number[] => {
  const res: number[] = [];
  for (const i of arr) {
    res.push(i * i);
  }
  return res;
};
export default squareAll;
