const allEven = (arr: number[]): number[] => {
  const res = [];
  for (const i of arr) {
    if (i % 2 === 0) {
      res.push(i);
    }
  }
  return res;
};
export default allEven;

// const allEven = (arr: number[]): number[] => filter (x => x % 2 === 0, arr);
// };
